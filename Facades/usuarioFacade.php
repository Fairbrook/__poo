<?php namespace Facades;

use \Models\usuario as Usuario;
class usuarioFacade extends \Core\baseEntity{
    public function __construct()
    {
        parent::__construct("usuario");
    }

    public function create($usuario){
        if(!is_object($usuario))return "Error: Not an object";
        $con = $this->connect();
        try{
            $con->beginTransaction();
            $id = $this->getNextId($con);
            $usuario->setId($id);
            $this->notReadQuery(
                "INSERT INTO {$this->getTable()} (id,name,pass) values (?,?,sha(?))",
                array($usuario->getId(),$usuario->getName(),$usuario->getPass()),
                $con
            );
            $con->commit();
            return $id;
        }catch(\Exception $e){
            $con->rollBack();
            return $e->getMessage();
        }finally{$con=null;}
    }
}