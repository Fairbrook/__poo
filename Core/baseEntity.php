<?php namespace Core;
	class baseEntity extends connection
	{
		private $table;

		public function __construct($table)
		{
			parent::__construct();
			$this->table = (string) $table;
			$this->table = filter_var($this->table,FILTER_SANITIZE_STRING);
		}

		protected function getTable(){
			return $this->table;
		}

		protected function getNextId($con=null){
			if(is_null($con))$db = $this->connect();
			else $db=$con;
			$result = array();
			try{
				$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$query = $db->prepare("SELECT MAX(id) as 'next' FROM {$this->table}");
				$status = $query->execute();
				if($status){
					$Obj=$query->fetch(\PDO::FETCH_OBJ);
					$result = ++$Obj->next;
				}else $result = 0;

			}catch(\Exception $e){
				$result = $e->getMessage();
			}finally{
				if(is_null($con))$db=null;
				return $result;
			}
		}

		protected function getAll($con=null){
			if(is_null($con))$db = $this->connect();
			else $db=$con;
			$result = array();
			try{
				$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$query = $db->prepare("SELECT * FROM {$this->table}");
				$query->execute();
				while ($row=$query->fetch(\PDO::FETCH_OBJ)) {
					$result[]=$row;
				}
			}catch(\Exception $e){
				$result = $e->getMessage();
			}finally{
				if(is_null($con))$db=null;
				return $result;
			}
		}

		protected function getById($id,$con=null){
			if(is_null($con))$db = $this->connect();
			else $db=$con;
			$id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);
			$result = array();
			try{
				$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$query = $db->prepare("SELECT * FROM {$this->table} WHERE id = :id");
				$query->bindParam(':id',$id,\PDO::PARAM_INT);
				$query->execute();
				$result = $query->fetch(\PDO::FETCH_OBJ);
			}catch(\Exception $e){
				$result = $e->getMessage($this->table);
			}finally{
				if(is_null($con))$db=null;
				return $result;
			}
		}
		
		protected function getBy($column,$value,$con=null){
			if(is_null($con))$db = $this->connect();
			else $db=$con;
			$column = filter_var($column,FILTER_SANITIZE_STRING);
			$value = filter_var($value,FILTER_SANITIZE_SPECIAL_STRING);
			$result = array();
			try{
				$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$query = $db->prepare("SELECT * FROM {$this->table} WHERE {$column} = :value");
				$query->bindParam(':value',$value);
				$query->execute();
				while ($row=$query->fetch(\PDO::FETCH_OBJ)) {
					$result[]=$row;
				}
			}catch(\Exception $e){
				$result = $e->getMessage();
			}finally{
				if(is_null($con))$db=null;
				return $result;
			};
		}

		protected function search($column,$value,$con=null){
			if(is_null($con))$db = $this->connect();
			else $db=$con;
			$column = filter_var($column,FILTER_SANITIZE_STRING);
			$result = array();
			try{
				$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$query = $db->prepare("SELECT * FROM {$this->table} WHERE {$column} LIKE :value");
				$query->bindParam(':value',$value,\PDO::PARAM_STR);
				$query->execute();
				while ($row=$query->fetch(\PDO::FETCH_OBJ)) {
					$result[]=$row;
				}
			}catch(\Exception $e){
				$result = $e->getMessage();
			}finally{
				if(is_null($con))$db=null;
				return $result;
			};
		}

		protected function delete($id,$con=null){
			if(is_null($con))$db = $this->connect();
			else $db=$con;
			$id = filter_var($id,FILTER_SANITIZE_NUMBER_INT);
			$result=false;
			try{
				$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$query = $db->prepare("DELETE FROM {$this->table} WHERE id = :id");
				$query->bindParam(':id',$id,\PDO::PARAM_INT);
				$result = $query->execute();
			}catch(\Exception $e){
				$result = $e->getMessage();
			}finally{
				if(is_null($con))$db=null;
				return $result;
			};
		}

		protected function readQuery($stmt,$params=array(),$con=null){
			$params = filter_var_array($params,FILTER_SANITIZE_STRING);
			if(is_null($con))$db = $this->connect();
			else $db=$con;
			$result=array();
			try{
				$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$query = $db->prepare($stmt);
				$query->execute($params);
				while ($row=$query->fetch(\PDO::FETCH_OBJ)) {
					$result[]=$row;
				}
			}catch(\Exception $e){
				$result = $e->getMessage();
			}finally{
				if(is_null($con))$db=null;
				return $result;
			}
		}

		protected function notReadQuery($stmt,$params=array(),$con=null){
			if(is_null($con))$db = $this->connect();
			else $db=$con;
			$params = filter_var_array($params,FILTER_SANITIZE_STRING);
			$result=false;
			try{
				$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
				$query = $db->prepare($stmt);
				$result = $query->execute($params);
			}catch(\Exception $e){
				$result = $e->getMessage();
			}finally{
				if(is_null($con))$db=null;
				return $result;
			};
		}
	}
?>