<?php namespace Core;
class connection{
	private $db_config;
	private $host, $user, $pass, $db, $charset;

	public function __construct(){
		$db_config = require_once 'Config/database.php';
		$this->host = $db_config['host'];
		$this->user = $db_config['user'];
		$this->pass = $db_config['pass'];
		$this->db = $db_config['db'];
		$this->charset = $db_config['charset'];
	}
	public function connect(){
		try{
			$con = new \PDO(
				"mysql:host=$this->host; dbname=$this->db",
				$this->user, 
				$this->pass,
				array(\PDO::ATTR_PERSISTENT => true)
			);
		}catch(PDOException $e){
			$con = $e->getMessage();
		}finally{return $con;}
	}
}
?>